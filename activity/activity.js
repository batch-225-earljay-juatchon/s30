db.fruits.insertMany([
		{
			name : "Apple",
			color : "Red",
			stock : 20,
			price: 40,
			supplier_id : 1,
			onSale : true,
			origin: [ "Philippines", "US" ]
		},

		{
			name : "Banana",
			color : "Yellow",
			stock : 15,
			price: 20,
			supplier_id : 2,
			onSale : true,
			origin: [ "Philippines", "Ecuador" ]
		},

		{
			name : "Kiwi",
			color : "Green",
			stock : 25,
			price: 50,
			supplier_id : 1,
			onSale : true,
			origin: [ "US", "China" ]
		},

		{
			name : "Mango",
			color : "Yellow",
			stock : 10,
			price: 120,
			supplier_id : 2,
			onSale : false,
			origin: [ "Philippines", "India" ]
		}     	
])

// 1
db.fruits.aggregate([
        {
            $match: { onSale: true }
        },
	{ $count: "fruitsonSale"}
]);


// 2
  db.fruits.aggregate ([
	{
		$match: { onSale: true } 
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                        price: { $sum: "$price"}
		}
	},
	{
		$project: { totalStocks: 20, _id:0 }
	}

])


  // 3

  db.fruits.aggregate ([
	{
		$match: { onSale: true } 
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                       ave_price: { $avg: "$price"}
		}
	},
	{
		$project: { totalStocks: 0, _id:0 }
	},
        {
                    $sort: { price: 1 }
            }

])


  // 4

  db.fruits.aggregate{[
    
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            max_price: { $max: "$price" },
     
        }
    },
{ 
        $project: { max_price: 1, _id: 1 }
    }
   
 ]} 


  // 5

  db.fruits.aggregate{[
    
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            min_price: { $min: "$price" },
     
        }
    },
{ 
        $project: {min_price: 1, _id: 1}
    }
   
 ]} 

